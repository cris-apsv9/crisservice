package es.upm.dit.apsv.cris.dao;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Session;

import es.upm.dit.apsv.cris.model.Publication;
import es.upm.dit.apsv.cris.model.Researcher;

public class ResearcherDAOImplementation implements ResearcherDAO {
	
	private static ResearcherDAOImplementation instance = null;
	private ResearcherDAOImplementation() {}
	//de esta forma solo hay un objeto de este tipo enn todo el programa
	
	//para acceder a este objeto tendremos que llamar a getinstance
	public static ResearcherDAOImplementation getInstance() {
		if (null ==instance) {
			instance = new ResearcherDAOImplementation();
		}
		return instance;
	}

	@Override
	public Researcher create(Researcher researcher) {
		Session session = SessionFactoryService.get().openSession();
		//try catch por si fallan las operaciones y  luego cerrramos la sesion
		try {
			session.beginTransaction();
			session.save(researcher);
			session.getTransaction().commit();
		}catch (Exception e){
			
		}finally {
			session.close();
		}
		return researcher;
	}

	@Override
	public Researcher read(String researcherId) {
		// TODO Auto-generated method stub
		Session session = SessionFactoryService.get().openSession();
		Researcher r = null;
		//try catch por si fallan las operaciones y  luego cerrramos la sesion
		try {
			session.beginTransaction();
			r = session.get(Researcher.class,researcherId);
			session.getTransaction().commit();
		}catch (Exception e){
		}finally {
			session.close();
		}
		return r;
	}

	@Override
	public Researcher update(Researcher researcher) {
		Session session = SessionFactoryService.get().openSession();
		//try catch por si fallan las operaciones y  luego cerrramos la sesion
		try {
			session.beginTransaction();
			session.saveOrUpdate(researcher);
			session.getTransaction().commit();
		}catch (Exception e){
			
		}finally {
			session.close();
		}
		return researcher;
	}

	@Override
	public Researcher delete(Researcher researcher) {
		Session session = SessionFactoryService.get().openSession();
		//try catch por si fallan las operaciones y  luego cerrramos la sesion
		try {
			session.beginTransaction();
			session.delete(researcher);
			session.getTransaction().commit();
		}catch (Exception e){
			
		}finally {
			session.close();
		}
		return researcher;
	}
	
	@Override
	public Researcher readByEmail(String email) {
		for(Researcher r : this.readAll())
			if(email.equals(r.getEmail()))
				return r;
		return null;
	}

	@Override
	public List<Researcher> readAll() {
		Session session = SessionFactoryService.get().openSession();
		List<Researcher> l = null;
		//try catch por si fallan las operaciones y  luego cerrramos la sesion
		try {
			session.beginTransaction();
			l =(List<Researcher>) session.createQuery("from Researcher").getResultList();
			session.getTransaction().commit();
		}catch (Exception e){
		}finally {
			session.close();
		}
		return l;
	}

}
