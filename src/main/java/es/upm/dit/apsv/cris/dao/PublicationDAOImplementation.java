package es.upm.dit.apsv.cris.dao;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Session;

import es.upm.dit.apsv.cris.model.Publication;
import es.upm.dit.apsv.cris.model.Researcher;

public class PublicationDAOImplementation implements PublicationDAO {
    private static PublicationDAOImplementation instance = null;

    private PublicationDAOImplementation() {
    }

    public static PublicationDAOImplementation getInstance() {
        if (instance == null)
            instance = new PublicationDAOImplementation();
        return instance;
    }

    @Override
    public Publication create(Publication p) {
        Session session = SessionFactoryService.get().openSession();
        try {
            session.beginTransaction();
            session.save(p);
            session.getTransaction().commit();
        } catch (Exception e) {
            throw e;
        } finally {
            session.close();
        }
        return p;

    }

    @Override
    public List<Publication> readAll() {

        Session session = SessionFactoryService.get().openSession();
        List<Publication> l = null;
        try {
        	session.beginTransaction();
			l = (List<Publication>) session.createQuery("from Publication").getResultList();
			session.getTransaction().commit();
        } catch (Exception e) {
        } finally {
            session.close();
        }
        return l;

    }

    @Override
    public Publication update(Publication p) {

        Session session = SessionFactoryService.get().openSession();
        try {
            session.beginTransaction();
            session.saveOrUpdate(p);
            session.getTransaction().commit();
        } catch (Exception e) {
            throw e;
        } finally {
            session.close();
        }
        return p;
    }

    @Override
    public Publication delete(Publication p) {
        Session session = SessionFactoryService.get().openSession();
        try {
            session.beginTransaction();
            session.delete(p);
            session.getTransaction().commit();
        } catch (Exception e) {
            throw e;
        } finally {
            session.close();
        }
        return p;

    }

    @Override
	public Publication read(String PublicationId) {
		Session session = SessionFactoryService.get().openSession();
		Publication r = null;
		try {
		session.beginTransaction();
			r = session.get(Publication.class, PublicationId);
			session.getTransaction().commit();
		}catch (Exception e){
		} finally {
			session.close();
		}
		return r;
	}
    public List<Publication> readAllPublications(String id){
		Session session = SessionFactoryService.get().openSession();
    	List<Publication> l = new ArrayList<Publication>();
    	try {
    		session.beginTransaction();
    		for(Publication p : this.readAll()) {
    			if (p.getAuthors().indexOf(id) > -1 ) {
    			l.add(p);
    		}}
    		session.getTransaction().commit();
		}catch (Exception e){
		} finally {
			session.close();
		}
    	return l;
    }
}
