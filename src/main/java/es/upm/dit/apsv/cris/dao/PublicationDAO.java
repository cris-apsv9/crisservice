package es.upm.dit.apsv.cris.dao;

import java.util.List;

import es.upm.dit.apsv.cris.model.Publication;

public interface PublicationDAO {
    public Publication create(Publication p);
    public Publication read(String PublicationId);
    public Publication update( Publication p);
    public Publication delete( Publication p);
    
    public List<Publication> readAll();
    public List<Publication> readAllPublications(String id);


}